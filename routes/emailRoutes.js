const express = require('express');
const upload = require('multer')({ dest: 'uploads/' });
const emailController = require('./../controllers/emailController');
const authController = require('./../controllers/authController');

// { mergeParams: true } = merging parameters on different routes
const router = express.Router({ mergeParams: true });

router
  .route('/')
  .get(authController.protect, emailController.getAllMessagesOnAUser)
  .post(
    authController.protect,
    upload.array('files'),
    emailController.createMessage
  );

// Get All Threads on a Message
router
  .route('/threads/:threadId')
  .post(authController.protect, emailController.getMessageThreads);

// Get All Emails on all User
router
  .route('/all')
  .post(authController.protect, emailController.getAllMessages);

// Get All Emails on a User
router
  .route('/message')
  .post(authController.protect, emailController.getAllMessagesOnAUser);

module.exports = router;
