const express = require('express');
const agendaController = require('./../controllers/agendaController');
const authController = require('./../controllers/authController');

const router = express.Router({ mergeParams: true });

// GET /user/1/agendas === GET /agendas
// POST /user/2/agendas === POST /agendas

router
  .route('/')
  .get(authController.protect, agendaController.getAllAgendas)
  .post(authController.protect, agendaController.createAgenda);

router
  .route('/:id')
  .get(authController.protect, agendaController.getAgenda)
  .patch(authController.protect, agendaController.updateAgenda)
  .delete(authController.protect, agendaController.deleteAgenda);

module.exports = router;
