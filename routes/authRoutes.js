const express = require('express');
const passport = require('passport');
const authController = require('./../controllers/authController');

// eslint-disable-next-line no-unused-vars
const _ = require('./../config/google.strategy');

const router = express.Router();

// Authentication with Google
router.get(
  '/google',
  passport.authenticate('google', {
    scope: [
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/user.birthday.read',
      'https://mail.google.com/'
    ],
    accessType: 'offline',
    prompt: 'consent'
  })
);

// Callback for google to redirect to
router.get(
  '/google/redirect',
  passport.authenticate('google'),
  authController.googleProtect
);

module.exports = router;
