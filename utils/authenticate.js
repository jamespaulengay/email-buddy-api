const { google } = require('googleapis');

// eslint-disable-next-line camelcase
module.exports = (access_token, refresh_token) => {
  const oAuth2Client = new google.auth.OAuth2(
    process.env.GOOGLE_CLIENT_ID,
    process.env.GOOGLE_CLIENT_SECRET
  );

  oAuth2Client.setCredentials({
    access_token,
    refresh_token
  });

  return oAuth2Client;
};
