const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');
const User = require('./../models/userModel');

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser(async (id, done) => {
  const user = await User.findOne({ _id: id }).lean();
  done(null, user);
});

passport.use(
  new GoogleStrategy(
    {
      //Passport Google Strategy Configurations
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.GOOGLE_CALLBACK_URL
    },
    async (accessToken, refreshToken, profile, done) => {
      // Passport Callback Function
      // 1) Check if user already exists in the database.
      console.log(profile.id);
      let user = await User.findOneAndUpdate(
        { googleId: profile.id },
        { accessToken, refreshToken },
        { new: true }
      ).lean();

      if (!user)
        user = await User.create({
          googleId: profile.id,
          firstName: profile.name.givenName,
          lastName: profile.name.familyName,
          email: profile.emails[0].value,
          accessToken,
          refreshToken
        });

      done(null, user);
    }
  )
);
