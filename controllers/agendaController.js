const Agenda = require('./../models/agendaModel');
const APIFeatures = require('./../utils/apiFeatures');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');

exports.getAllAgendas = catchAsync(async (req, res, next) => {
  // Initialize filter object
  let filter = {};
  // For nested routes, if there is a userId, then create a filter object.
  if (req.params.userId) filter = { user: req.params.userId };

  const features = new APIFeatures(Agenda.find(filter), req.query)
    .filter()
    .sort()
    .limitFields()
    .paginate();

  const agendas = await features.query;

  // SEND RESPONSE
  res.status(200).json({
    status: 'success',
    results: agendas.length,
    data: {
      agendas
    }
  });
});

exports.getAgenda = catchAsync(async (req, res, next) => {
  const agenda = await Agenda.findById(req.params.id);

  if (!agenda) {
    return next(new AppError('No agenda found with that ID', 404));
  }

  res.status(200).json({
    status: 'success',
    data: {
      agenda
    }
  });
});

exports.createAgenda = catchAsync(async (req, res, next) => {
  // Allow nested  routes
  if (!req.body.user) req.body.user = req.user.id;

  const newAgenda = await Agenda.create(req.body);

  res.status(201).json({
    status: 'success',
    data: {
      agenda: newAgenda
    }
  });
});

exports.updateAgenda = catchAsync(async (req, res, next) => {
  const agenda = await Agenda.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!agenda) {
    return next(new AppError('No agenda found with that ID', 404));
  }

  res.status(200).json({
    status: 'success',
    data: {
      agenda
    }
  });
});

exports.deleteAgenda = catchAsync(async (req, res, next) => {
  const agenda = await Agenda.findByIdAndDelete(req.params.id);

  if (!agenda) {
    return next(new AppError('No agenda found with that ID', 404));
  }

  res.status(204).json({
    status: 'success',
    data: null
  });
});
