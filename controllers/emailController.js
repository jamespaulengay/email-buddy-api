const path = require('path');
const fsPromises = require('fs').promises;

const { google } = require('googleapis');
const Mailgun = require('mailgun.js');
const formData = require('form-data');
const parseMessage = require('gmail-api-parse-message');

const catchAsync = require('./../utils/catchAsync');
const authenticate = require('./../utils/authenticate');

// Instantiating Mailgun once.
const mailgun = new Mailgun(formData);
const client = mailgun.client({
  username: 'api',
  key: process.env.MAILGUN_API_KEY
});

exports.getAllMessagesOnAUser = catchAsync(async (req, res, next) => {
  // eslint-disable-next-line no-unused-vars
  // let filter = {};
  // if (req.params.userId) filter = { user: req.params.userId };

  if (!req.query.q) req.query.q = '';
  if (!req.query.pageToken) req.query.pageToken = '';
  if (!req.query.maxResults) req.query.maxResults = 20;
  if (!req.query.labelIds) req.query.labelIds = [];
  if (!req.query.includeSpamTrash) req.query.includeSpamTrash = false;

  const auth = authenticate(req.body.accessToken, req.body.refreshToken);
  const gmail = google.gmail({ version: 'v1', auth });
  let labelIds = [];

  if (req.query.labelIds && req.query.length > 0) {
    // Convert query to an array of strings
    labelIds = [req.query.labelIds.split(',').join(' ')].join(' ').split(' ');
  }

  if (req.query.includeSpamTrash === 'true') {
    // Convert string to boolean
    req.query.includeSpamTrash = true;
  }

  const results = await gmail.users.messages.list({
    userId: 'me',
    q: req.query.q,
    maxResults: +req.query.maxResults,
    labelIds: labelIds || req.query.labelIds,
    pageToken: req.query.pageToken,
    includeSpamTrash: req.query.includeSpamTrash
  });

  // For pagination
  const { nextPageToken, resultSizeEstimate } = results.data;

  const messages = await results.data.messages.map(async msg => {
    const message = await gmail.users.messages.get({
      userId: 'me',
      id: msg.id
    });

    const parsedMessage = parseMessage(message.data);
    return {
      mail: parsedMessage.id,
      threadId: parsedMessage.threadId,
      postedAt: parsedMessage.headers.date,
      label: parsedMessage.labelIds,
      from: parsedMessage.headers.from,
      subject: parsedMessage.headers.subject,
      body: parsedMessage.snippet,
      user_id: req.user._id,
      attachment: parsedMessage.attachments
    };
  });

  const emails = await Promise.all(messages);

  res.status(200).json({
    status: 'success',
    results: emails.length,
    data: {
      emails,
      pageToken: nextPageToken,
      resultSizeEstimate
    }
  });
});

exports.getAllMessages = catchAsync(async (req, res, next) => {
  // eslint-disable-next-line no-unused-vars
  // let filter = {};
  // if (req.params.userId) filter = { user: req.params.userId };

  if (!req.query.q) req.query.q = '';
  if (!req.query.pageToken) req.query.pageToken = '';
  if (!req.query.maxResults) req.query.maxResults = 20;
  if (!req.query.labelIds) req.query.labelIds = [];
  if (!req.query.includeSpamTrash) req.query.includeSpamTrash = false;

  const accessTokens = req.body.accessToken;
  const refreshTokens = req.body.refreshToken;

  // TODO: To be fixed.
  // let nextPageToken = '';
  // let resultSizeEstimate = '';

  if (Array.isArray(accessTokens) && Array.isArray(refreshTokens)) {
    const allMessages = await accessTokens.map(async (accessToken, index) => {
      const refreshToken = refreshTokens[index];

      const auth = authenticate(accessToken, refreshToken);
      const gmail = google.gmail({ version: 'v1', auth });
      let labelIds = [];

      if (req.query.labelIds && req.query.length > 0) {
        // Convert query to an array of strings
        labelIds = [req.query.labelIds.split(',').join(' ')]
          .join(' ')
          .split(' ');
      }

      if (req.query.includeSpamTrash === 'true') {
        // Convert string to boolean
        req.query.includeSpamTrash = true;
      }

      const results = await gmail.users.messages.list({
        userId: 'me',
        q: req.query.q,
        maxResults: +req.query.maxResults,
        labelIds: labelIds || req.query.labelIds,
        pageToken: req.query.pageToken,
        includeSpamTrash: req.query.includeSpamTrash
      });

      // For pagination
      // eslint-disable-next-line prefer-destructuring
      // nextPageToken = results.data.nextPageToken;
      // eslint-disable-next-line prefer-destructuring
      // resultSizeEstimate = results.data.resultSizeEstimate;

      let messages = await results.data.messages.map(async msg => {
        const message = await gmail.users.messages.get({
          userId: 'me',
          id: msg.id
        });

        const parsedMessage = parseMessage(message.data);

        return {
          mail: parsedMessage.id,
          threadId: parsedMessage.threadId,
          postedAt: parsedMessage.headers.date,
          label: parsedMessage.labelIds,
          from: parsedMessage.headers.from,
          subject: parsedMessage.headers.subject,
          body: parsedMessage.snippet,
          user_id: req.user._id,
          attachment: parsedMessage.attachments
        };
      });

      messages = await Promise.all(messages);
      return messages;
    });

    let emails = await Promise.all(allMessages);
    emails = emails.flat();

    res.status(200).json({
      status: 'success',
      results: emails.length,
      data: {
        emails
      }
    });
  }
});

exports.getMessageThreads = catchAsync(async (req, res, next) => {
  const { threadId } = req.params;

  const auth = authenticate(req.body.accessToken, req.body.refreshToken);
  const gmail = google.gmail({ version: 'v1', auth });

  const threads = await gmail.users.threads.get({ userId: 'me', id: threadId });
  let { messages } = threads.data;

  messages = messages.map(message => parseMessage(message));

  res.status(200).json({
    status: 'success',
    results: messages.length,
    data: {
      threads: messages
    }
  });
});

//TODO: Delete uploaded files
exports.createMessage = catchAsync(async (req, res, next) => {
  // Allow nested routes if they are not define in the request body
  if (!req.body.user) req.body.user = req.user.id;

  // Acquire uploaded files and message data
  const {
    files: attachments,
    body: { data: msg }
  } = req;

  // Initialize file to undefined
  let files;

  // Converting attachment into a valid object to be used in mailgun.
  if (attachments) {
    files = attachments.map(async attachment => {
      const filename = attachment.originalname;
      const slash = attachment.path.replace('\\', '/');
      const filePath = path.join(`${__dirname}/../`, slash);
      const data = await fsPromises.readFile(filePath);

      return {
        filename,
        data
      };
    });

    // Resolve all files inside the files array
    files = Promise.all(files);
    files = await files;
  }

  const message = JSON.parse(msg);

  // const auth = authenticate(req.user.accessToken, req.user.refresh_token);

  const messageData = {
    from: `${req.user.firstName} ${req.user.lastName} <${req.user.email} >`,
    to: message.recipientEmailAddress,
    subject: message.subject,
    text: message.message,
    attachment: files
  };

  // Returns { status, id, message } and sends the message
  const mmessage = await client.messages.create(
    process.env.DOMAIN,
    messageData
  );

  return res.status(201).json({
    status: 'success',
    message: mmessage.message
  });
});
