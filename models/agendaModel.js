const mongoose = require('mongoose');

const agendaSchema = mongoose.Schema(
  {
    title: {
      required: true,
      type: String,
      trim: true
    },
    description: {
      required: true,
      type: String,
      trim: true
    },
    dateofagenda: {
      type: Date,
      default: Date.now()
    },
    user: {
      type: mongoose.Schema.ObjectId,
      ref: 'User',
      required: [true, 'Agenda must belong to a user']
    }
  },
  {
    timestamps: true
  }
);

agendaSchema.pre(/^find/, function(next) {
  this.populate({
    path: 'user',
    select: 'email firstName lastName'
  });
  next();
});

const Agenda = mongoose.model('Agenda', agendaSchema);

module.exports = Agenda;
